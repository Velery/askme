function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function (xhr, settings) {
        var csrftoken = getCookie('csrftoken');
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

//$(".starclass").click(function () {
//    $(this).addClass("active");
//});

$(".button-collapse").sideNav();

$(".down").click(function () {
    var qId = $(this).attr('js-id');
    var self = this;
    $.post("/like/", {'id': qId, 'value': 0, 'type': 0},
        function (data) {
            if (data.message == 'okay') {
                var rate = parseInt($(self).parent().parent().find('.forLikes')[0].innerHTML) - 1;
                $(self).parent().parent().find('.forLikes')[0].innerHTML = rate;
                $($(self).children('i')[0]).addClass("active");
            } else {
                alert("You have already voted for this question!");
            }
        });
});

$(".up").click(function () {
    var qId = $(this).attr('js-id');
    var self = this;
    $.post("/like/", {'id': qId, 'value': 1, 'type': 0},
        function (data) {
            if (data.message == 'okay') {
                var rate = parseInt($(self).parent().parent().find('.forLikes')[0].innerHTML) + 1;
                $(self).parent().parent().find('.forLikes')[0].innerHTML = rate;
                console.log($(self).children('i')[0]);
                $($(self).children('i')[0]).addClass("active");
            } else {
                alert("You have already voted for this question!");
            }
        });
});

$(".ans-down").click(function () {
    $(this).addClass("active");
    var qId = $(this).attr('js-id');
    var self = this;
    $.post("/like/", {'id': qId, 'value': 0, 'type': 1},
        function (data) {
            if (data.message == 'okay') {
                var rate = parseInt($(self).parent().parent().find('.forLikes')[0].innerHTML) - 1;
                $(self).parent().parent().find('.forLikes')[0].innerHTML = rate;
                $($(self).children('i')[0]).addClass("active");
            } else {
                alert("You have already voted for this answer!");
            }
        });
});

$(".ans-up").click(function () {
    var qId = $(this).attr('js-id');
    var self = this;
    $.post("/like/", {'id': qId, 'value': 1, 'type': 1},
        function (data) {
            if (data.message == 'okay') {
                var rate = parseInt($(self).parent().parent().find('.forLikes')[0].innerHTML) + 1;
                $(self).parent().parent().find('.forLikes')[0].innerHTML = rate;
                $($(self).children('i')[0]).addClass("active");
            } else {
                alert("You have already voted for this answer!");
            }
        });
});

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

$("input[type=checkbox]").click(function () {
    var ansId = $(this).attr('js-id');
    var queId = $(this).attr('js-q-id');
    console.log({'ansId': ansId, 'questId': queId});
    $.post("/correct/", {'ansId': ansId, 'questId': queId},
        function (data) {
            if (data.message == 'okay') {
                alert("it's yours!");
            } else {
                alert("it's not yours!");
            }
        });
})