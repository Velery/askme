from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.http import Http404
from ask.utils import paginate, make_likes
import json
from django.http import JsonResponse
from ask.models import Profile, QuestManager, Question, Answer, Tag, RateAnswer, RateQuestion
from ask.forms import UserForms, RegistrationForm, SettingsForm, NewQuestion, NewAnswer
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required


def helloworld(request):
    output = '<b>Hello World</b><br />'
    if request.method == 'GET':
        output += 'Get query: '
        for x in request.GET:
            output += x + '='
            output += request.GET[x] + ' '
    else:
        output += 'Post query: '
        for x in request.POST:
            output += x + '='
            output += request.POST[x] + ' '
    return HttpResponse("<html><body>%s</body></html>" % output)


def home(request, sort):
    questions = []
    status = {'active'}
    if sort == 'hot':
        questions = Question.objects.best()
    else:
        questions = Question.objects.hot()

    objects_page, paginator = paginate(questions, request)
    objects_page = Question.objects.get_answ_n_tags(objects_page)
    profile = None
    usr = None
    if request.user.is_authenticated():
        try:
            usr = request.user
            profile = Profile.objects.get(user=request.user)
        except Profile.DoesNotExist:
            profile = Profile.objects.create(rate=0, user_id=request.user.id)

    return render(request, 'index.html', {'objects': objects_page, 'status': status, 'sort': sort,
                                          'profile': profile, 'user': usr})


@login_required(login_url='view-login')
def tag(request, tagname):
    questions = Question.objects.by_tag(tagname)

    objects_page, paginator = paginate(questions, request)
    objects_page = Question.objects.get_answ_n_tags(objects_page)
    try:
        profile = Profile.objects.get(user=request.user)
    except Profile.DoesNotExist:
        profile = Profile.objects.create(rate=0, user_id=request.user.id)
    return render(request, 'index.html', {'objects': objects_page, 'profile': profile})


def log_in(request):
    user = request.user
    raise_error = False
    value = request.REQUEST.get('next', '')
    if request.method == 'POST':
        form = UserForms(request.POST)
        if form.is_valid():
            form.username = request.POST['username']
            form.password = request.POST['password']
            user = authenticate(username=form.username, password=form.password)
            if user is not None:
                login(request, user)
                return HttpResponseRedirect(request.POST.get('next', ''))
            else:
                raise_error = True
    else:
        form = UserForms()
    return render(request, 'login.html', {'form': form, 'next': value,
                                          'raise_error': raise_error, 'user': user})


def log_out(request):
    value = request.REQUEST.get('next', '')
    logout(request)
    return HttpResponseRedirect(value)


def registration(request):
    user = None
    if request.method == 'POST':
        form = RegistrationForm(request.POST, request.FILES)
        if form.is_valid():
            form.login = request.POST['login']
            form.email = request.POST['email']
            form.password = request.POST['password']
            form.password = request.POST['pass2']
            if request.FILES:
                form.image = request.FILES['image']
            else:
                form.image = ''
            RegistrationForm.create_user(form)
            user = authenticate(username=form.login, password=form.password)
            if user is not None:
                login(request, user)
                return home(request, 'hot')
    else:
        form = RegistrationForm()
    return render(request, 'registration.html', {'form': form, 'user': user})


@login_required(login_url='view-login')
def settings(request):
    profile = Profile.objects.get(user=request.user)
    if request.method == 'POST':
        form = SettingsForm(request.POST, request.FILES)
        if form.is_valid():
            if request.FILES:
                form.image = request.FILES['image']
                profile.avatar = request.FILES['image']
                profile.save()
            if request.POST['login'] != '':
                request.user.username = request.POST['login']
            if request.POST['email'] != '':
                request.user.email = request.POST['email']
            request.user.save()
            form = SettingsForm()
    else:
        form = SettingsForm()
    return render(request, 'settings.html', {'form': form, 'profile': profile})


@login_required(login_url='view-login')
def question(request, quest_id=1):
    try:
        question = Question.objects.get(id=quest_id)
    except Question.DoesNotExist:
        raise Http404

    try:
        profile = Profile.objects.get(user=request.user)
    except Profile.DoesNotExist:
        profile = Profile.objects.create(rate=0, user_id=request.user.id)

    if question.author == profile:
        this_users_quest = True
    else:
        this_users_quest = False

    question.tag = Question.objects.by_quest(question)
    answers = Answer.objects.get_answers(question)
    objects_page, paginator = paginate(answers, request)
    try:
        correct_answer = Answer.objects.get(quest=question, is_right=True).id
    except Answer.DoesNotExist:
        correct_answer = None

    if request.method == 'POST':
        form = NewAnswer(request.POST)
        if form.is_valid():
            form.body = request.POST['body']
            profile = Profile.objects.get(user=request.user)
            page, id = form.create_answer(profile, question)
            return HttpResponseRedirect('/question/' + str(quest_id) + '?page=' + str(page) + '#' + str(id))
    else:
        form = NewAnswer()
    return render(request, 'question.html', {'question': question, 'objects': objects_page,
                                             'form': form, 'correct': correct_answer, 'profile': profile,
                                             'this_users_quest': this_users_quest})


@login_required(login_url='view-login')
def new_quest(request):
    try:
        profile = Profile.objects.get(user=request.user)
    except Profile.DoesNotExist:
        profile = Profile.objects.create(rate=0, user_id=request.user.id)

    if request.method == 'POST':
        form = NewQuestion(request.POST)
        if form.is_valid():
            form.title = request.POST['title']
            form.body = request.POST['body']
            profile = Profile.objects.get(user=request.user)
            quest = form.create_question(profile)
            return redirect('view-question', quest)
    else:
        form = NewQuestion()
    return render(request, 'ask.html', {'form': form, 'profile': profile})


@login_required(login_url='view-login')
def like(request):
    obj_id = 0
    value = 0
    type = 0
    try:
        obj_id = int(request.POST.get('id'))
        value = int(request.POST.get('value'))
        type = int(request.POST.get('type'))
    except ValueError, TypeError:
        bad_data = {
            'status': 'false',
            'message': "bad"
        }
        return JsonResponse(bad_data)

    profile = Profile.objects.get(user=request.user)
    return make_likes(profile, obj_id, value, type)


@login_required(login_url='view-login')
def correct_answ(request):
    bad_data = {
        'status': 'false',
        'message': "bad"
    }
    good_data = {
        'status': 'true',
        'message': 'okay'
    }
    profile = Profile.objects.get(user=request.user)
    try:
        ans_id = request.POST.get('ansId')
        q_id = request.POST.get('questId')
    except ValueError, TypeError:
        return JsonResponse(bad_data)

    try:
        quest = Question.objects.get(pk=q_id, author=profile)
    except Question.DoesNotExist:
        return JsonResponse(bad_data)

    all_answers = Answer.objects.filter(quest=quest)
    for a in all_answers:
        a.is_right = False
        a.save()
    answer = Answer.objects.get(pk=ans_id)
    answer.is_right = True
    answer.save()
    return JsonResponse(good_data)
