__author__ = 'iampsg'
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import JsonResponse
from ask.models import Question, RateQuestion, Answer, RateAnswer
from django.db.models import F


def paginate(objects_list, request):
    p = Paginator(objects_list, 3)
    page = request.GET.get('page')
    try:
        objects_page = p.page(page)
    except PageNotAnInteger:
        objects_page = p.page(1)
    except EmptyPage:
        objects_page = p.page(p.num_pages)

    return objects_page, p


def make_likes(profile, obj_id, value, type):
    bad_data = {
        'status': 'false',
        'message': "bad"
    }
    good_data = {
        'status': 'true',
        'message': 'okay'
    }
    if type == 0:
        question = 0
        try:
            question = Question.objects.get(pk=obj_id)
        except Question.DoesNotExist:
            return JsonResponse(bad_data)

        try:
            rate = RateQuestion.objects.get(who=profile, question=question)
        except RateQuestion.DoesNotExist:
            if value == 0:
                Question.objects.filter(pk=obj_id).update(rate=F('rate') - 1)
                # question.rate -= 1
            else:
                Question.objects.filter(pk=obj_id).update(rate=F('rate') + 1)
                # question.rate += 1
            rate_quest = RateQuestion.objects.create(who=profile, question=question)
            rate_quest.save()
            return JsonResponse(good_data)
        return JsonResponse(bad_data)

    else:
        answ = 0
        try:
            answ = Answer.objects.get(pk=obj_id)
        except Answer.DoesNotExist:
            return JsonResponse(bad_data)

        try:
            rate = RateAnswer.objects.get(who=profile, answer=answ)
        except RateAnswer.DoesNotExist:
            if value == 0:
                Answer.objects.filter(pk=obj_id).update(rate=F('rate') - 1)
                # answ.rate -= 1
            else:
                Answer.objects.filter(pk=obj_id).update(rate=F('rate') + 1)
                # answ.rate += 1
            # answ.save()
            rate_answ = RateAnswer.objects.create(who=profile,answer=answ)
            rate_answ.save()
            return JsonResponse(good_data)
        return JsonResponse(bad_data)