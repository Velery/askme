# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ask', '0005_auto_20151115_1208'),
    ]

    operations = [
        migrations.AlterField(
            model_name='answer',
            name='is_right',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='answer',
            name='rate',
            field=models.IntegerField(default=0),
        ),
    ]
