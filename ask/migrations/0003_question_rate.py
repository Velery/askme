# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ask', '0002_rateanswer_ratequestion'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='rate',
            field=models.IntegerField(default=0),
        ),
    ]
