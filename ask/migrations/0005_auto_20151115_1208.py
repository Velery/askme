# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ask', '0004_answer_quest'),
    ]

    operations = [
        migrations.AlterField(
            model_name='answer',
            name='quest',
            field=models.ForeignKey(default=1, to='ask.Question'),
        ),
    ]
