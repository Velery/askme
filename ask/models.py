from django.db import models
from django.contrib.auth.models import User



class Profile(models.Model):
    user = models.OneToOneField(User)
    avatar = models.ImageField()
    rate = models.IntegerField()


class QuestManager(models.Manager):
    def best(self):
        #TODO: turn to desc
        return self.order_by('-rate')

    def hot(self):
        return self.order_by('-id')

    def get_answ_n_tags(self, questions):
        for q in questions:
            answ = Answer.objects.filter(quest = q)
            q.count = answ.count()
            q.tag = q.tags.all()
        return questions

    def by_tag(self, tag_name):
        return Tag.objects.filter(tag = tag_name)[0].question_set.all()

    def by_quest(self, question):
        return question.tags.all()


class AnswerManager(models.Manager):
    def get_answers(self, question):
        return self.filter(quest = question)


class Question(models.Model):
    header = models.CharField(max_length=100)
    body = models.TextField()
    author = models.ForeignKey('Profile')
    create_date = models.DateField(auto_now=True)
    tags = models.ManyToManyField('Tag')
    rate = models.IntegerField(default = 0)
    objects = QuestManager()


class Answer(models.Model):
    body = models.TextField()
    quest = models.ForeignKey('Question', default = 1)
    author = models.ForeignKey('Profile')
    create_date = models.DateField(auto_now=True)
    is_right = models.BooleanField(default=False)
    rate = models.IntegerField(default=0)
    objects = AnswerManager()


class Tag(models.Model):
    tag = models.CharField(max_length = 20)


class RateAnswer(models.Model):
    who = models.ForeignKey('Profile')
    answer = models.ForeignKey('Answer')


class RateQuestion(models.Model):
    who = models.ForeignKey('Profile')
    question = models.ForeignKey('Question')

    def find_question(self,quest):
        return RateQuestion.object.filter(quetion=quest)