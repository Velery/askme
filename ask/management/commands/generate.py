__author__ = 'iampsg'
from django.core.management.base import BaseCommand, CommandError
from faker import Factory
import random

class Command(BaseCommand):
    def handle(self, *args, **options):
        num_of_users = 0
        #11000
        fake = Factory.create()
        f = open('auth_user', 'w')
        f1 = open('profile', 'w')
        usr_names = {}
        while len(usr_names) < num_of_users:
            usr_names[fake.user_name()] = 1

        cntr = 1
        for usr_name in usr_names:
            f1.write(str(fake.random_number(2)) + ";" + str(cntr) + "\n")
            f.write(str(usr_name) + ";" + str(fake.first_name()) + ";" + str(fake.last_name())
                    + ";" + fake.email() + ";" + str(fake.date_time()) + "\n")
            cntr += 1
        #load data local infile 'profile' into table ask_profile  COLUMNS TERMINATED BY ';' (rate, user_id);
        #load data local infile 'auth_user' into table auth_user  COLUMNS TERMINATED BY ';' (username, first_name, last_name, email);

        num_of_tags = 0
        #150
        f2 = open('tags', 'w')
        tags = {}
        while len(tags) < num_of_tags:
            tags[fake.word()] = 1

        for tag in tags:
            f2.write(tag + "\n")

        #load data local infile 'tags' into table ask_tag COLUMNS TERMINATED BY ';' (tag);

        num_of_quest = 0
        #100100
        f3 = open('questions', 'w')

        for i in range(num_of_quest):
            f3.write( fake.paragraph(1) + ";" + fake.paragraph(7) + ";" + str(random.randint(1,11000))
                      + ";" + str(fake.date_time()) + ";" + str(fake.random_number(2)) + "\n")
        #load data local infile 'questions' into table ask_question COLUMNS TERMINATED BY ';' (header, body, author_id, create_date, rate);


        num_of_quest_tag = 0
        #300300
        f4 = open('quest_tag','w')

        for i in range(num_of_quest_tag):
            f4.write(str(random.randint(1, 100100)) + ";" + str(random.randint(1, 150)) + "\n")
        #load data local infile 'quest_tag' into table ask_question_tags COLUMNS TERMINATED BY ';' (question_id, tag_id);

        num_of_ans = 0
        #1000100
        f5 = open('answers', 'w')

        for i in range(num_of_ans):
            f5.write(fake.paragraph(3) + ";" + str(fake.date_time()) + ";" + str(fake.random_number(1))
                     + ";" + str(random.randint(1, 100100)) + ";" + str(random.randint(1, 11000)) + "\n" )
        #load data local infile 'answers' into table ask_answer COLUMNS TERMINATED BY ';' (body, create_date, rate, quest_id, author_id);