__author__ = 'iampsg'
from django import forms
from django.db import models
from django.contrib.auth.models import User
from ask.models import Profile, Question, Tag, Answer
from django.db import IntegrityError
from django.forms import ModelForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from ask.utils import paginate

class UserForms(forms.Form):
    username = forms.CharField(label='Username', max_length=10)
    password = forms.CharField(widget=forms.PasswordInput())

    def clean(self):
        cleaned_data = super(UserForms, self).clean()
        passw = cleaned_data.get('password')
        try:
            u = User.objects.get(username=cleaned_data.get('username'))
        except User.DoesNotExist:
            self.add_error('username', "Sorry, wrong username or password!")
            return
        if any(self.errors):
            self.add_error('password', "Sorry, wrong username or password!")
            return


class RegistrationForm(forms.Form):
    login = forms.CharField(label='Login', max_length=20)
    email = forms.CharField(widget=forms.EmailInput())
    password = forms.CharField(widget=forms.PasswordInput())
    pass2 = forms.CharField(widget=forms.PasswordInput())
    image = forms.ImageField(required=False)

    def create_user(self):
        user = User.objects.create_user(self.login, self.email, self.password)
        user.save()
        profile = Profile.objects.create(rate=0, user_id=user.id, avatar=self.image)
        profile.save()
        return user

    def clean(self):
        cleaned_data = super(RegistrationForm, self).clean()
        pass1 = cleaned_data.get("password")
        pass2 = cleaned_data.get("pass2")
        if pass1 != pass2:
            msg = "Passwords are not equal"
            self.add_error('password', msg)
            self.add_error('pass2', msg)
        try:
            u = User.objects.get(username=cleaned_data.get('login'))
        except User.DoesNotExist:
            return
        self.add_error('login', "Username already exists")
        if any(self.errors):
            return


class SettingsForm(forms.Form):
    login = forms.CharField(label='Login', max_length=20, required=False)
    email = forms.CharField(widget=forms.EmailInput(), required=False)
    image = forms.ImageField(required=False)

    def clean(self):
        cleaned_data = super(SettingsForm, self).clean()
        if cleaned_data.get('login') == '':
            return
        try:
            usr = User.objects.get(username=cleaned_data.get('login'))
        except User.DoesNotExist:
            return
        self.add_error('login', 'Username already exists')


class NewQuestion(forms.Form):
    title = forms.CharField(label='Title', max_length=50)
    body = forms.CharField(widget=forms.Textarea())
    tags = forms.CharField(label='Tags')

    def create_question(self, profile):
        quest = Question.objects.create(header=self.title, body=self.body, author=profile)
        for tag in self.tag_objects:
            quest.tags.add(tag)
        quest.save()
        return quest.id

    def clean(self):
        cleaned_data = super(NewQuestion, self).clean()
        if cleaned_data.get('title') == '':
            self.add_error('title', "Title is empty")
        if cleaned_data.get('body') == '':
            self.add_error('body', "Please, add question body")

        tags_list = cleaned_data.get('tags').split(' ')
        self.tag_objects = list()
        for tag in tags_list:
            tag_obj=0
            try:
                tag_obj = Tag.objects.get(tag=tag)
            except Tag.DoesNotExist:
                tag_obj = Tag.objects.create(tag=tag)
            self.tag_objects.append(tag_obj)


class NewAnswer(forms.Form):
    body = forms.CharField(widget=forms.Textarea())

    def create_answer(self, profile, question):
        answer = Answer.objects.create(quest=question, author=profile, body=self.body)
        answer.save()
        answers = Answer.objects.get_answers(question)
        paginator = Paginator(answers, 3)
        for p in paginator.page_range:
            if answer in paginator.page(p):
                return p,answer.id
        return 1,answer.id

    def clean(self):
        cleaned_data = super(NewAnswer, self).clean()
        if cleaned_data.get('body') == '':
            self.add_error('body', "Can't add empty answer")