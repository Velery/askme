from django.conf.urls import include, url
from django.contrib import admin
from ask import views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.home, {'sort': 'no'}, name='view-new-quest'),
    url(r'^hot/$', views.home, {'sort': 'hot'}, name='view-hot-quest'),
    url(r'^tag/(?P<tagname>[a-z]+)/$', views.tag, name='view-by-tag'),
    url(r'^login/$', views.log_in, name='view-login'),
    url(r'^registration/', views.registration, name='view-registration'),
    url(r'^settings/$', views.settings, name='view-settings'),
    url(r'^question/(?P<quest_id>[0-9]+)/$', views.question, name='view-question'),
    url(r'^ask/$', views.new_quest, name='view-add-new-quest'),
    url(r'^settings/$', views.settings, name='view-settings'),
    url(r'^logout/$', views.log_out, name='view-logout'),
    url(r'^like/$', views.like ,name='view-like'),
    url(r'^correct/$', views.correct_answ, name='view-correct')
]
